1. Buat database
create database myshop;

2. Buat tabel di dalam database
tabel users: 
	create table users(
    -> id int(8) auto_increment,
    -> name varchar(255),
    -> email varchar(255),
    -> password varchar(255),
    -> primary key(id)
    -> );
tabel items: 
	create table items(
    -> id int(8) auto_increment,
    -> name varchar(255),
    -> description varchar(255),
    -> price int,
    -> stock int,
    -> category_id int(8),
    -> primary key(id),
    -> foreign key(category_id) references categories(id)
    -> );
tabel categories: 
	create table categories(
    -> id int(8) auto_increment,
    -> name varchar(255),
    -> primary key(id)
    -> );

3. Memasukkan data pada tabel
data users: insert into users(name,email,password) values("John Doe","john@doe.com","john123"),("Jane Doe","jane@doe.com","jenita123");
data categories: insert into categories(name) values("Gadget"),("Cloth"),("Men"),("Women"),("Branded");
data items: insert into items(name,description,price,stock,category_id) value("Sumsang B50","Hape keren dari merek Sumsang",4000000,100,1),("Uniklooh","Baju keren dari brand ternama",500000,50,2),("IMHO Watch","Jam tangan anak yang jujur banget",2000000,10,1);

4. Mengambil data dari database
a. select id,name,email from users;
b. 
==> select * from items where price>1000000;
==> select * from items where name like "%watch%";
c. select items.id, items.name, items.description, items.price, items.stock, items.category_id, categories.name from items inner join categories on items.category_id=categories.id;

5. Mengubah data dari database
update items set price = 2500000 where name = "Sumsang B50";